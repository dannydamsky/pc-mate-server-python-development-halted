from bluetooth import BluetoothError

from program.bluetooth_server import BluetoothServer

__server_name__ = "PC Mate Server"
__uuid__ = "b35a16de-258b-4031-8a8c-0f51439abe59"


def main():
    my_server = BluetoothServer(__server_name__, __uuid__)
    print(my_server)
    __run_server__(my_server)


def __run_server__(my_server):
    try:
        while True:
            my_server.start()
    except BluetoothError:
        print("Connection disconnected, restarting server...")
        __run_server__(my_server)


main()
