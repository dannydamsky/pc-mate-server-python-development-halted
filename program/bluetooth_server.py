import bluetooth

from program.inputs.pc_con import PcController


def __init_server_socket__(listen_to_amount):
    server_socket = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
    server_socket.bind(("", bluetooth.PORT_ANY))
    server_socket.listen(listen_to_amount)
    return server_socket


class BluetoothServer:
    """The Bluetooth server that bridges between the Android device and the computer."""
    __server_name__ = None
    __uuid__ = None
    __server_socket__ = None
    __input_socket__ = None
    __controller__ = PcController()

    def __init__(self, server_name, uuid):
        self.__server_name__ = server_name
        self.__uuid__ = uuid

    def __str__(self):
        return """
Bluetooth Server
    Server Name: %s
    UUID: %s
""" % (self.__server_name__, self.__uuid__)

    def __del__(self):
        self.__close_sockets__()

    def start(self):
        while True:
            self.__server_socket__ = __init_server_socket__(1)
            self.__init_service__()
            self.__input_socket__, address = self.__server_socket__.accept()
            print("Got connection with", address)
            self.__receive_input_from_socket__()
            self.__close_sockets__()

    def __init_service__(self):
        bluetooth.advertise_service(self.__server_socket__, self.__server_name__,
                                    service_id=self.__uuid__,
                                    service_classes=[self.__uuid__, bluetooth.SERIAL_PORT_CLASS],
                                    profiles=[bluetooth.SERIAL_PORT_PROFILE])

    def __receive_input_from_socket__(self):
        while True:
            data = self.__input_socket__.recv(1024)
            if not data:
                break
            self.__controller__.send(data.decode('UTF-8'))

    def __close_sockets__(self):
        if self.__input_socket__ is not None:
            self.__input_socket__.close()
        if self.__server_socket__ is not None:
            self.__server_socket__.close()
