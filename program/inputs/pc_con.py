from datetime import datetime

from program.inputs.types.pc_input import PcInput


class PcController:
    __input__ = None

    def __init__(self):
        pass

    def __str__(self):
        if self.__input__ is not None:
            return """
PC Controller
    Currently Using: %s
""" % self.__input__.TYPE
        else:
            return """
PC Controller
    Currently not using any input.            
"""

    def __del__(self):
        self.__input__ = None

    def send(self, event):
        print("""
New event at %s
Parsing event: %s
""" % (str(datetime.now()), event))
        if self.__get_input_mode__(event):
            print("Detected: New Input Mode")
        elif self.__input__ is not None:
            self.__apply__(event)
        else:
            print("Unable to parse event")

    def __get_input_mode__(self, input_mode):
        __input__ = PcInput.get_correct_input(input_mode)
        if __input__ is not None:
            self.__input__ = __input__
        return __input__ is not None

    def __apply__(self, command):
        print("Detected: New Command")
        self.__input__.apply(command)
