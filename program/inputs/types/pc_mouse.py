import pynput.keyboard
from pynput.mouse import Controller, Button

from program.inputs.types.pc_input import PcInput, Constants


class PcMouse(PcInput):
    """This class is in charge of outputting mouse events."""
    TYPE = Constants.IDENTIFIER_MOUSE
    __mouse__ = Controller()
    __keyboard__ = pynput.keyboard.Controller()
    __button__ = None

    def __init__(self):
        PcInput.__init__(self)

    def apply(self, command):
        self.__do_action__(command)

    # Determines the mouse action and applies it to the computer mouse.
    # Each entry looks like this: "X;Y;"
    # Therefore, last item in the array will always be empty.
    def __do_action__(self, coordinates):
        commands_arr = coordinates.split(";")
        half_length = int((len(commands_arr) - 1) / 2)
        for i in range(half_length):
            self.__apply_action__(commands_arr, i)

    def __apply_action__(self, commands_arr, i):
        x_str = commands_arr[i]
        y_str = commands_arr[i + 1]
        self.__apply_coordinates__(x_str, y_str)

    def __apply_coordinates__(self, x_str, y_str):
        if self.__moves_mouse__(x_str, y_str):
            pass
        elif x_str == Constants.ACTION_MOUSE_SCROLL:
            self.__mouse__.scroll(0, int(y_str))
        elif y_str == Constants.ACTION_MOUSE_SCROLL:
            self.__mouse__.scroll(int(x_str), 0)
        elif x_str == Constants.ACTION_MOUSE_ZOOM_IN:
            self.__zoom_in__()
        elif x_str == Constants.ACTION_MOUSE_ZOOM_OUT:
            self.__zoom_out__()
        elif x_str == Constants.ACTION_MOUSE_LEFT:
            self.__press__(Button.left)
        elif x_str == Constants.ACTION_MOUSE_MIDDLE:
            self.__press__(Button.middle)
        elif x_str == Constants.ACTION_MOUSE_RIGHT:
            self.__press__(Button.right)
        elif x_str == Constants.ACTION_MOUSE_RELEASE:
            self.__mouse__.release(self.__button__)
        else:
            print("Error in __apply_coordinates__\nX:%s\nY:%s") % (x_str, y_str)

    def __moves_mouse__(self, x_str, y_str):
        try:
            x = int(x_str)
            y = int(y_str)
            self.__mouse__.move(x, y)
            return True
        except ValueError:
            return False

    def __press__(self, button):
        self.__button__ = button
        self.__mouse__.press(button)

    def __zoom_in__(self):
        self.__keyboard_ctrl_action__('+')

    def __zoom_out__(self):
        self.__keyboard_ctrl_action__('-')

    def __keyboard_ctrl_action__(self, action):
        with (self.__keyboard__.pressed(pynput.keyboard.Key.ctrl)):
            self.__keyboard__.press(action)
            self.__keyboard__.release(action)
