class PcInput:
    """Abstract class whose children control inputs"""
    TYPE = None

    def __init__(self):
        pass

    def apply(self, command):
        pass

    def __str__(self):
        return self.TYPE

    @staticmethod
    def get_correct_input(input_type):
        if input_type == PcInput.TYPE:
            return PcInput()
        else:
            from program.inputs.types.pc_keyboard import PcKeyboard
            if input_type == PcKeyboard.TYPE:
                return PcKeyboard()
            else:
                from program.inputs.types.pc_mouse import PcMouse
                if input_type == PcMouse.TYPE:
                    return PcMouse()

        return None


class Constants:
    IDENTIFIER_MOUSE = "%V9jDY5#m%"
    IDENTIFIER_KEYBOARD = "v3&qB&uSS%"

    ACTION_MOUSE_LEFT = "le"
    ACTION_MOUSE_MIDDLE = "mi"
    ACTION_MOUSE_RIGHT = "ri"
    ACTION_MOUSE_RELEASE = "re"
    ACTION_MOUSE_SCROLL = "sc"
    ACTION_MOUSE_ZOOM_IN = "zi"
    ACTION_MOUSE_ZOOM_OUT = "zo"

    def __init__(self):
        pass
