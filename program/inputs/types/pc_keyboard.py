from pynput.keyboard import Controller

from program.inputs.types.pc_input import PcInput, Constants


class PcKeyboard(PcInput):
    """This class is in charge of outputting keyboard events."""
    TYPE = Constants.IDENTIFIER_KEYBOARD
    __keyboard__ = Controller()

    def __init__(self):
        PcInput.__init__(self)

    def apply(self, command):
        self.__keyboard__.type(command)
